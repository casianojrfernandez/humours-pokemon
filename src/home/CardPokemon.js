import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { usePokemon } from "../context/PokemonProvider";
import CardSkeleton from "./CardSkeleton";

export default function CardPokemon({ pokemon }) {
  const { TYPE, dispatch } = usePokemon();
  const [poke, setPoke] = useState("");
  useEffect(() => {
    axios.get(pokemon.url).then((response) => setPoke(response.data));
  }, [pokemon]);

  if (!poke) {
    return <CardSkeleton />;
  }

  return (
    <Card
      onClick={() => {
        dispatch({ type: TYPE.setPokemonSpotLight, payload: { poke } });
        dispatch({ type: TYPE.setOpenDrawer, payload: true });
      }}
    >
      <CardActionArea style={{ minHeight: "140px", minWidth: "140px" }}>
        <div style={{ textAlign: "center" }}>
          <img
            alt={poke.name}
            width="auto"
            height="auto"
            src={poke.sprites.front_default}
          />
        </div>
        <CardContent>
          <Typography
            variant="body1"
            align="center"
            gutterBottom
            style={{ textTransform: "capitalize" }}
          >
            {poke.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActionArea></CardActionArea>
    </Card>
  );
}
