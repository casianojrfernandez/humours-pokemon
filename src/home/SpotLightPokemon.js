import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { usePokemon } from "../context/PokemonProvider";
import Carousel from "react-material-ui-carousel";
import { Paper } from "@material-ui/core";

export default function SpotLightPokemon() {
  const { state } = usePokemon();
  const poke = state.pokemonSpotLight.poke;
  const joke = state.jokes.sort(() => 0.5 - Math.random())[1];

  const backgroundTint = [
    "rgba(161, 245, 161, 0.8)",
    "rgba(236, 183, 132, 0.8)",
    "rgba(141, 236, 132, 0.8)",
    "rgba(182, 148, 245, 0.8)",
  ].sort(() => 0.5 - Math.random());
  if (!poke) return null;
  return (
    <Card style={{ minHeight: "30vw", overflow: "scroll" }}>
      <CardActionArea style={{ minHeight: "120px" }}>
        <>
          <div style={{ backgroundColor: backgroundTint }}>
            <Carousel
              interval={1000}
              timeout={{ appear: 100, enter: 100, exit: 100 }}
              indicators={false}
              navButtonsAlwaysInvisible={true}
            >
              {[
                poke.sprites.front_default,
                poke.sprites.front_default,
                poke.sprites.back_default,
                poke.sprites.back_default,
                poke.sprites.front_shiny,
                poke.sprites.front_shiny,
                poke.sprites.back_shiny,
                poke.sprites.back_shiny,
              ].map((mon, idx) => (
                <Paper
                  key={idx}
                  style={{
                    backgroundColor: backgroundTint,
                    minHeight: "47vh",
                    transform: idx % 2 !== 0 ? "scaleX(-1)" : "",
                  }}
                >
                  <img alt={idx} width="100%" height="auto" src={mon} />
                </Paper>
              ))}
            </Carousel>
          </div>
        </>
        <CardContent>
          <Typography
            variant="h4"
            align="center"
            gutterBottom
            style={{ textTransform: "capitalize" }}
          >
            {poke.name}
          </Typography>
          <Typography style={{ fontStyle: "italic" }}>{joke.setup}</Typography>
          <Typography align="right" style={{ fontWeight: "bold" }}>
            <b> Humour: </b>
            {joke.punchline}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActionArea>
        <CardContent
          style={{
            border: "1px solid blue",
            borderRadius: "5%",
            margin: "5px",
          }}
        >
          <Typography align="center" variant="h5">
            Stats
          </Typography>
          <Typography align="justify">
            My name is
            <span style={{ textTransform: "capitalize" }}> {poke.name}</span>,
            virginity -{poke.base_experience}, height {poke.height}, weight{" "}
            {poke.weight}, strongness {poke.order}. Thank you!
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
