import React, { useEffect } from "react";
import { basePokemonApi, baseJokesApi } from "../shared/baseApi";
import { Grid } from "@material-ui/core";
import { usePokemon } from "../context/PokemonProvider";
import CardPokemon from "./CardPokemon";
import DrawerPokemon from "./DrawerPokemon";
import SpotLightPokemon from "./SpotLightPokemon";

export default function Home() {
  const { TYPE, state, dispatch } = usePokemon();

  useEffect(() => {
    basePokemonApi
      .get("/pokemon")
      .then((response) =>
        dispatch({ type: TYPE.setPokemonList, payload: response.data.results })
      );
    baseJokesApi
      .get("/")
      .then((response) =>
        dispatch({ type: TYPE.setJokes, payload: response.data })
      );
  }, [TYPE, dispatch]);

  return (
    <div>
      <h2>Home</h2>
      <Grid container spacing={2}>
        {state.pokemonList &&
          state.pokemonList.map((pokemon) => (
            <Grid item key={pokemon.name}>
              <CardPokemon pokemon={pokemon} />
            </Grid>
          ))}
      </Grid>
      <DrawerPokemon>
        {state.pokemonSpotLight && <SpotLightPokemon />}
      </DrawerPokemon>
    </div>
  );
}
