import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";

export default function CardSkeleton() {
  return (
    <Skeleton
      animation="wave"
      style={{ minHeight: "140px", minWidth: "140px" }}
    />
  );
}
