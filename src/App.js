import { BrowserRouter } from "react-router-dom";
import PokemonProvider from "./context/PokemonProvider";
import GlobalRoutes from "./shared/GlobalRoutes";
import NavigationBar from "./shared/NavigationBar";
import "./bodyStyle.css";

function App() {
  return (
    <BrowserRouter>
      <PokemonProvider>
        <NavigationBar>
          <GlobalRoutes />
        </NavigationBar>
      </PokemonProvider>
    </BrowserRouter>
  );
}

export default App;
