import axios from "axios";

const fetchLimit = 24;

export const basePokemonApi = axios.create({
  baseURL: "https://pokeapi.co/api/v2/",
  params: {
    limit: fetchLimit,
  },
});

export const baseJokesApi = axios.create({
  baseURL: `https://us-central1-dadsofunny.cloudfunctions.net/DadJokes/random/jokes/50`,
});
