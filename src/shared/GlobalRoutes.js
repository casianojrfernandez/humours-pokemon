import React from "react";
import { Route, Switch } from "react-router-dom";
import About from "../about/About";
import Home from "../home/Home";

export default function GlobalRoutes() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/about" component={About} />
    </Switch>
  );
}
