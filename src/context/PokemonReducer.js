import { useReducer } from "react";

const TYPE = {
  setPokemonList: "setPokemonList",
  setPokemonSpotLight: "setPokemonSpotLight",
  setOpenDrawer: "setOpenDrawer",
  setJokes: "setJokes",
};

const initialState = {
  pokemonList: [],
  pokemonSpotLight: "",
  openDrawer: false,
  jokes: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case TYPE.setPokemonList:
      return { ...state, pokemonList: action.payload };
    case TYPE.setPokemonSpotLight:
      return { ...state, pokemonSpotLight: action.payload };
    case TYPE.setOpenDrawer:
      return { ...state, openDrawer: action.payload };
    case TYPE.setJokes:
      return { ...state, jokes: action.payload };
    default:
      return state;
  }
};

export default function PokemonReducer() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return { TYPE, state, dispatch };
}
