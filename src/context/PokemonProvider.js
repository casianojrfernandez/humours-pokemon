import React, { createContext, useContext } from "react";
import PokemonReducer from "./PokemonReducer";
const PokemonContext = createContext(null);

export const usePokemon = () => useContext(PokemonContext);

export default function PokemonProvider({ children }) {
  const { TYPE, state, dispatch } = PokemonReducer();
  const value = { TYPE, state, dispatch };
  return (
    <PokemonContext.Provider value={value}>{children}</PokemonContext.Provider>
  );
}
